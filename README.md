# Install dvb-t2 on linux with PCTV Systems tripleStick T2 (292e)

To watch TV on a Linux System using a TV Systems tripleStick T2 you need a specific firmware and a xspf file to watch tv with vlc.

## firmware

Download the firmware:

`sudo wget http://palosaari.fi/linux/v4l-dvb/firmware/Si2168/dvb-demod-si2168-02.fw -O /lib/firmware/dvb-demod-si2168-02.fw`

`sudo wget http://palosaari.fi/linux/v4l-dvb/firmware/Si2168/Si2168-B40/d8da7ff67cd56cd8aa4e101aea45e052/dvb-demod-si2168-b40-01.fw -O /lib/firmware/dvb-demod-si2168-b40-01.fw`
## w_scan

[dvbt w_scan and arch linux](https://wiki.archlinux.org/index.php/DVB-T)

## vlc

watch the xslp file in the repository!